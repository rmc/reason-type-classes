open Apply;

module type BindBase = {
  include Apply;
  let bind: (t('a), 'a => t('b)) => t('b);
};

module type Bind = {
  include BindBase;
  let (>>=): (t('a), 'a => t('b)) => t('b);
  let bindFlipped: ('a => t('b), t('a)) => t('b);
  let (=<<): ('a => t('b), t('a)) => t('b);
  let join: t(t('a)) => t('a);
  let composeKleisli: ('a => t('b), 'b => t('c), 'a) => t('c);
  let (>=>): ('a => t('b), 'b => t('c), 'a) => t('c);
  let composeKleisliFlipped: ('b => t('c), 'a => t('b), 'a) => t('c);
  let (<=<): ('b => t('c), 'a => t('b), 'a) => t('c);
  let ifM: (t(bool), t('a), t('a)) => t('a);
};

module MakeBind = (F: BindBase) : (Bind with type t('a) = F.t('a)) => {
  open Function;
  include F;
  let (>>=) = bind;
  let bindFlipped = (x, f) => f >>= x;
  let (=<<) = bindFlipped;
  let join = m => m >>= id;
  let composeKleisli = (f, g, x) => f(x) >>= g;
  let (>=>) = composeKleisli;
  let composeKleisliFlipped = (g, f) => f >=> g;
  let (<=<) = composeKleisliFlipped;
  let ifM = (condition, f, g) => condition >>= (c => c ? f : g);
};

module TestBind = (F: Bind) => {
  include TestApply(F);
  open F;
  let testBindAssociativity = (f, g, x) => {
    let left = x >>= f >>= g;
    let right = x >>= (k => f(k) >>= g);
    left == right;
  };
};

module OptionBind: Bind with type t('a) = option('a) =
  MakeBind(
    {
      include OptionApply;
      let bind = (m, f) =>
        switch m {
        | Some(x) => f(x)
        | _ => None
        };
    }
  );

module ListBind: Bind with type t('a) = list('a) =
  MakeBind(
    {
      include ListApply;
      let rec bind = (m: list('a), f: 'a => list('b)) : list('b) =>
        switch m {
        | [] => []
        | [head, ...tail] => List.concat([f(head), bind(tail, f)])
        };
    }
  );
