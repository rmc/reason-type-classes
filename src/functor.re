module type FunctorBase = {type t('a); let map: ('a => 'b, t('a)) => t('b);};

module type Functor = {
  include FunctorBase;
  let (<$>): ('a => 'b, t('a)) => t('b);
  let mapFlipped: (t('a), 'a => 'b) => t('b);
  let (<#>): (t('a), 'a => 'b) => t('b);
  let voidRight: ('a, t('b)) => t('a);
  let (<$): ('a, t('b)) => t('a);
  let voidLeft: (t('a), 'b) => t('b);
  let ($>): (t('a), 'b) => t('b);
  let flap: (t('a => 'b), 'a) => t('b);
  let (<@>): (t('a => 'b), 'a) => t('b);
};

module MakeFunctor = (F: FunctorBase) : (Functor with type t('a) = F.t('a)) => {
  open Function;
  include F;
  let (<$>) = map;
  let mapFlipped = (ft, f) => f <$> ft;
  let (<#>) = mapFlipped;
  let voidRight = x => map(const(x));
  let (<$) = voidRight;
  let voidLeft = (ft, x) => const(x) <$> ft;
  let ($>) = voidLeft;
  let flap = (ft, x) => (f => f(x)) <$> ft;
  let (<@>) = flap;
};

module TestFunctor = (F: Functor) => {
  open Function;
  open F;
  let testIdentity = x => id <$> x == x;
  let testComposition = (f, g, xs) =>
    map(f << g, xs) == (map(f) << map(g))(xs);
};

module OptionFunctor: Functor with type t('a) = option('a) =
  MakeFunctor(
    {
      type t('a) = option('a);
      let map = (f, value) =>
        switch value {
        | Some(x) => Some(f(x))
        | _ => None
        };
    }
  );

module ListFunctor: Functor with type t('a) = list('a) =
  MakeFunctor(
    {
      type t('a) = list('a);
      let map = List.map;
    }
  );
