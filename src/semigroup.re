module type SemigroupBase = {type t; let append: (t, t) => t;};

module type Semigroup = {include SemigroupBase; let (++): (t, t) => t;};

module MakeSemigroup = (M: SemigroupBase) : (Semigroup with type t = M.t) => {
  include M;
  let (++) = append;
};

module TestSemigroup = (M: Semigroup) => {
  open M;
  let testAssociativity = (x, y, z) => (x ++ y) ++ z == x ++ y ++ z;
};

module StringSemigroup: Semigroup with type t = string =
  MakeSemigroup(
    {
      type t = string;
      let append = Pervasives.(++);
    }
  );
