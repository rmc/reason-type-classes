open Apply;

module type ApplicativeBase = {include Apply; let pure: 'a => t('a);};

module type Applicative = {
  include ApplicativeBase;
  let when_: (bool, t(unit)) => t(unit);
  let unless: (bool, t(unit)) => t(unit);
};

module MakeApplicative =
       (F: ApplicativeBase)
       : (Applicative with type t('a) = F.t('a)) => {
  include F;
  let when_ = (x, f) => x ? f : pure();
  let unless = x => when_(! x);
};

module TestApplicative = (F: Applicative) => {
  include TestApply(F);
  open Function;
  open F;
  let testApplicativeIdentity = x => pure(id) <*> x == x;
  let testApplicativeComposition = (f, g, xs) => {
    let left = pure((<<)) <*> f <*> g <*> xs;
    let right = f <*> (g <*> xs);
    left == right;
  };
  let testApplicativeHomomorphism = (f, x) =>
    pure(f) <*> pure(x) == pure(f(x));
  let testApplicativeInterchange = (f, g) => {
    let left = f <*> pure(g);
    let right = pure(h => h(g)) <*> f;
    left == right;
  };
};

module ListApplicative: Applicative with type t('a) = list('a) =
  MakeApplicative(
    {
      include ListApply;
      let pure = x => [x];
    }
  );

module OptionApplicative: Applicative with type t('a) = option('a) =
  MakeApplicative(
    {
      include OptionApply;
      let pure = x => Some(x);
    }
  );
