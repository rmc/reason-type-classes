open Semigroup;

module type MonoidBase = {include Semigroup; let mempty: t;};

module type Monoid = {
  include MonoidBase;
  let power: (t, int) => t;
  let guard: (bool, t) => t;
};

module MakeMonoid = (M: MonoidBase) : (Monoid with type t = M.t) => {
  include M;
  let rec power = (x, n) =>
    switch n {
    | n when n <= 0 => mempty
    | 1 => x
    | n => power(x ++ x, n - 1)
    };
  let guard = (cond, m) => cond ? m : mempty;
};

module TestMonoid = (M: Monoid) => {
  include TestSemigroup(M);
  open M;
  let testLeftIdentity = x => mempty ++ x == x;
  let testRightIdentity = x => x ++ mempty == x;
};

module StringMonoid: Monoid with type t = string =
  MakeMonoid(
    {
      include StringSemigroup;
      let mempty = "";
    }
  );

module Dual = {
  module MakeDual = (M: Monoid) => {
    include M;
    let append = (x, y) => append(y, x);
  };
  module StringMonoidDual: Monoid with type t = string =
    MakeDual(StringMonoid);
};

module Additive = {
  type additive('a) =
    | Additive('a);
  module IntMonoidAdditive: Monoid with type t = additive(int) =
    MakeMonoid(
      {
        type t = additive(int);
        module SemigroupAdditive: Semigroup with type t = t =
          MakeSemigroup(
            {
              type t = additive(int);
              let append = (Additive(x), Additive(y)) => Additive(x + y);
            }
          );
        include (SemigroupAdditive: Semigroup with type t := t);
        let mempty = Additive(0);
      }
    );
};

module Multiplicative = {
  type multiplicative('a) =
    | Multiplicative('a);
  module IntMonoidMultiplicative: Monoid with type t = multiplicative(int) =
    MakeMonoid(
      {
        type t = multiplicative(int);
        module SemigroupMultiplicative: Semigroup with type t = t =
          MakeSemigroup(
            {
              type t = multiplicative(int);
              let append = (Multiplicative(x), Multiplicative(y)) =>
                Multiplicative(x * y);
            }
          );
        include (SemigroupMultiplicative: Semigroup with type t := t);
        let mempty = Multiplicative(1);
      }
    );
};

module Conj = {
  type conj('a) =
    | Conj('a);
  module BoolMonoidConj: Monoid with type t = conj(bool) =
    MakeMonoid(
      {
        type t = conj(bool);
        module SemigroupConj: Semigroup with type t = t =
          MakeSemigroup(
            {
              type t = conj(bool);
              let append = (Conj(x), Conj(y)) => Conj(x && y);
            }
          );
        include (SemigroupConj: Semigroup with type t := t);
        let mempty = Conj(true);
      }
    );
};

module Disj = {
  type disj('a) =
    | Disj('a);
  module BoolMonoidDisj: Monoid with type t = disj(bool) =
    MakeMonoid(
      {
        type t = disj(bool);
        module SemigroupDisj: Semigroup with type t = t =
          MakeSemigroup(
            {
              type t = disj(bool);
              let append = (Disj(x), Disj(y)) => Disj(x || y);
            }
          );
        include (SemigroupDisj: Semigroup with type t := t);
        let mempty = Disj(false);
      }
    );
};
