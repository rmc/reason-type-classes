open Functor;

module type ApplyBase = {
  include Functor;
  let apply: (t('a => 'b), t('a)) => t('b);
};

module type Apply = {
  include ApplyBase;
  let (<*>): (t('a => 'b), t('a)) => t('b);
  let applyFirst: (t('a), t('b)) => t('a);
  let (<*): (t('a), t('b)) => t('a);
  let applySecond: (t('a), t('b)) => t('b);
  let (*>): (t('a), t('b)) => t('b);
};

module MakeApply = (F: ApplyBase) : (Apply with type t('a) = F.t('a)) => {
  open Function;
  include F;
  let (<*>) = apply;
  let applyFirst = (f, j) => const <$> f <*> j;
  let (<*) = applyFirst;
  let applySecond = (f, j) => const(id) <$> f <*> j;
  let (*>) = applySecond;
};

module TestApply = (F: Apply) => {
  include TestFunctor(F);
  open Function;
  open F;
  let testAssociativeComposition = (f, g, xs) => {
    let left = (<<) <$> f <*> g <*> xs;
    let right = f <*> (g <*> xs);
    left == right;
  };
};

module OptionApply: Apply with type t('a) = option('a) =
  MakeApply(
    {
      include OptionFunctor;
      let apply = (f, x) =>
        switch (f, x) {
        | (Some(f), Some(x)) => Some(f(x))
        | _ => None
        };
    }
  );

module ListApply: Apply with type t('a) = list('a) =
  MakeApply(
    {
      include ListFunctor;
      let apply = (fs, xs) => (f => f <$> xs) <$> fs |> List.concat;
    }
  );
