// Generated by BUCKLESCRIPT VERSION 2.1.0, PLEASE EDIT WITH CARE
'use strict';

var List                     = require("bs-platform/lib/js/list.js");
var Curry                    = require("bs-platform/lib/js/curry.js");
var Monoid$ReasonTypeClasses = require("./monoid.bs.js");

function MakeFoldable(F) {
  return /* module */[
          /* foldr */F[0],
          /* foldl */F[1]
        ];
}

function MakeFoldableMonoid(F) {
  return (function (MN) {
      var foldl = F[1];
      var fold = function (f) {
        return Curry._3(foldl, (function (b, a) {
                      return Curry._2(MN[/* append */0], b, a);
                    }), MN[/* mempty */2], f);
      };
      return /* module */[
              /* foldr */F[0],
              /* foldl */foldl,
              /* M */MN,
              /* fold */fold
            ];
    });
}

function foldr(f, b, oa) {
  return List.fold_right(f, oa, b);
}

var ListFoldable = /* module */[
  /* foldr */foldr,
  /* foldl */List.fold_left
];

function MakeListFoldableMonoid(MN) {
  var fold = function (f) {
    return List.fold_left((function (b, a) {
                  return Curry._2(MN[/* append */0], b, a);
                }), MN[/* mempty */2], f);
  };
  return /* module */[
          /* foldr */foldr,
          /* foldl */List.fold_left,
          /* M */MN,
          /* fold */fold
        ];
}

function fold(f) {
  return List.fold_left((function (b, a) {
                return Curry._2(Monoid$ReasonTypeClasses.StringMonoid[/* append */0], b, a);
              }), Monoid$ReasonTypeClasses.StringMonoid[/* mempty */2], f);
}

var ListFoldableStringMonoid = /* module */[
  /* foldr */foldr,
  /* foldl */List.fold_left,
  /* M */Monoid$ReasonTypeClasses.StringMonoid,
  /* fold */fold
];

var MN = Monoid$ReasonTypeClasses.Dual[/* StringMonoidDual */1];

function fold$1(f) {
  return List.fold_left((function (b, a) {
                return Curry._2(MN[/* append */0], b, a);
              }), MN[/* mempty */2], f);
}

var ListFoldableStringMonoidDual = /* module */[
  /* foldr */foldr,
  /* foldl */List.fold_left,
  /* M */MN,
  /* fold */fold$1
];

exports.MakeFoldable                 = MakeFoldable;
exports.MakeFoldableMonoid           = MakeFoldableMonoid;
exports.ListFoldable                 = ListFoldable;
exports.MakeListFoldableMonoid       = MakeListFoldableMonoid;
exports.ListFoldableStringMonoid     = ListFoldableStringMonoid;
exports.ListFoldableStringMonoidDual = ListFoldableStringMonoidDual;
/* Monoid-ReasonTypeClasses Not a pure module */
