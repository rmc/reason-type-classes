// Generated by BUCKLESCRIPT VERSION 2.1.0, PLEASE EDIT WITH CARE
'use strict';

var List                       = require("bs-platform/lib/js/list.js");
var Curry                      = require("bs-platform/lib/js/curry.js");
var Caml_obj                   = require("bs-platform/lib/js/caml_obj.js");
var Apply$ReasonTypeClasses    = require("./apply.bs.js");
var Function$ReasonTypeClasses = require("./function.bs.js");

function MakeBind(F) {
  var bind = F[16];
  var bindFlipped = function (x, f) {
    return Curry._2(bind, f, x);
  };
  var join = function (m) {
    return Curry._2(bind, m, Function$ReasonTypeClasses.id);
  };
  var composeKleisli = function (f, g, x) {
    return Curry._2(bind, Curry._1(f, x), g);
  };
  var composeKleisliFlipped = function (g, f) {
    return (function (param) {
        return Curry._2(bind, Curry._1(f, param), g);
      });
  };
  var ifM = function (condition, f, g) {
    return Curry._2(bind, condition, (function (c) {
                  if (c !== 0) {
                    return f;
                  } else {
                    return g;
                  }
                }));
  };
  return /* module */[
          /* map */F[0],
          /* <$> */F[1],
          /* mapFlipped */F[2],
          /* <#> */F[3],
          /* voidRight */F[4],
          /* <$ */F[5],
          /* voidLeft */F[6],
          /* $> */F[7],
          /* flap */F[8],
          /* <@> */F[9],
          /* apply */F[10],
          /* <*> */F[11],
          /* applyFirst */F[12],
          /* <* */F[13],
          /* applySecond */F[14],
          /* *> */F[15],
          /* bind */bind,
          /* >>= */bind,
          /* bindFlipped */bindFlipped,
          /* =<< */bindFlipped,
          /* join */join,
          /* composeKleisli */composeKleisli,
          /* >=> */composeKleisli,
          /* composeKleisliFlipped */composeKleisliFlipped,
          /* <=< */composeKleisliFlipped,
          /* ifM */ifM
        ];
}

function TestBind(F) {
  var include = Apply$ReasonTypeClasses.TestApply([
        F[0],
        F[1],
        F[2],
        F[3],
        F[4],
        F[5],
        F[6],
        F[7],
        F[8],
        F[9],
        F[10],
        F[11],
        F[12],
        F[13],
        F[14],
        F[15]
      ]);
  var testBindAssociativity = function (f, g, x) {
    var left = Curry._2(F[/* >>= */17], Curry._2(F[/* >>= */17], x, f), g);
    var right = Curry._2(F[/* >>= */17], x, (function (k) {
            return Curry._2(F[/* >>= */17], Curry._1(f, k), g);
          }));
    return Caml_obj.caml_equal(left, right);
  };
  return /* module */[
          /* testIdentity */include[0],
          /* testComposition */include[1],
          /* testAssociativeComposition */include[2],
          /* testBindAssociativity */testBindAssociativity
        ];
}

function bind(m, f) {
  if (m) {
    return Curry._1(f, m[0]);
  } else {
    return /* None */0;
  }
}

function bindFlipped(x, f) {
  return bind(f, x);
}

function join(m) {
  return bind(m, Function$ReasonTypeClasses.id);
}

function composeKleisli(f, g, x) {
  return bind(Curry._1(f, x), g);
}

function composeKleisliFlipped(g, f) {
  return (function (param) {
      return bind(Curry._1(f, param), g);
    });
}

function ifM(condition, f, g) {
  return bind(condition, (function (c) {
                if (c !== 0) {
                  return f;
                } else {
                  return g;
                }
              }));
}

var OptionBind_000 = /* map */Apply$ReasonTypeClasses.OptionApply[0];

var OptionBind_001 = /* <$> */Apply$ReasonTypeClasses.OptionApply[1];

var OptionBind_002 = /* mapFlipped */Apply$ReasonTypeClasses.OptionApply[2];

var OptionBind_003 = /* <#> */Apply$ReasonTypeClasses.OptionApply[3];

var OptionBind_004 = /* voidRight */Apply$ReasonTypeClasses.OptionApply[4];

var OptionBind_005 = /* <$ */Apply$ReasonTypeClasses.OptionApply[5];

var OptionBind_006 = /* voidLeft */Apply$ReasonTypeClasses.OptionApply[6];

var OptionBind_007 = /* $> */Apply$ReasonTypeClasses.OptionApply[7];

var OptionBind_008 = /* flap */Apply$ReasonTypeClasses.OptionApply[8];

var OptionBind_009 = /* <@> */Apply$ReasonTypeClasses.OptionApply[9];

var OptionBind_010 = /* apply */Apply$ReasonTypeClasses.OptionApply[10];

var OptionBind_011 = /* <*> */Apply$ReasonTypeClasses.OptionApply[11];

var OptionBind_012 = /* applyFirst */Apply$ReasonTypeClasses.OptionApply[12];

var OptionBind_013 = /* <* */Apply$ReasonTypeClasses.OptionApply[13];

var OptionBind_014 = /* applySecond */Apply$ReasonTypeClasses.OptionApply[14];

var OptionBind_015 = /* *> */Apply$ReasonTypeClasses.OptionApply[15];

var OptionBind = /* module */[
  OptionBind_000,
  OptionBind_001,
  OptionBind_002,
  OptionBind_003,
  OptionBind_004,
  OptionBind_005,
  OptionBind_006,
  OptionBind_007,
  OptionBind_008,
  OptionBind_009,
  OptionBind_010,
  OptionBind_011,
  OptionBind_012,
  OptionBind_013,
  OptionBind_014,
  OptionBind_015,
  /* bind */bind,
  /* >>= */bind,
  /* bindFlipped */bindFlipped,
  /* =<< */bindFlipped,
  /* join */join,
  /* composeKleisli */composeKleisli,
  /* >=> */composeKleisli,
  /* composeKleisliFlipped */composeKleisliFlipped,
  /* <=< */composeKleisliFlipped,
  /* ifM */ifM
];

function bind$1(m, f) {
  if (m) {
    return List.concat(/* :: */[
                Curry._1(f, m[0]),
                /* :: */[
                  bind$1(m[1], f),
                  /* [] */0
                ]
              ]);
  } else {
    return /* [] */0;
  }
}

function bindFlipped$1(x, f) {
  return bind$1(f, x);
}

function join$1(m) {
  return bind$1(m, Function$ReasonTypeClasses.id);
}

function composeKleisli$1(f, g, x) {
  return bind$1(Curry._1(f, x), g);
}

function composeKleisliFlipped$1(g, f) {
  return (function (param) {
      return bind$1(Curry._1(f, param), g);
    });
}

function ifM$1(condition, f, g) {
  return bind$1(condition, (function (c) {
                if (c !== 0) {
                  return f;
                } else {
                  return g;
                }
              }));
}

var ListBind_000 = /* map */Apply$ReasonTypeClasses.ListApply[0];

var ListBind_001 = /* <$> */Apply$ReasonTypeClasses.ListApply[1];

var ListBind_002 = /* mapFlipped */Apply$ReasonTypeClasses.ListApply[2];

var ListBind_003 = /* <#> */Apply$ReasonTypeClasses.ListApply[3];

var ListBind_004 = /* voidRight */Apply$ReasonTypeClasses.ListApply[4];

var ListBind_005 = /* <$ */Apply$ReasonTypeClasses.ListApply[5];

var ListBind_006 = /* voidLeft */Apply$ReasonTypeClasses.ListApply[6];

var ListBind_007 = /* $> */Apply$ReasonTypeClasses.ListApply[7];

var ListBind_008 = /* flap */Apply$ReasonTypeClasses.ListApply[8];

var ListBind_009 = /* <@> */Apply$ReasonTypeClasses.ListApply[9];

var ListBind_010 = /* apply */Apply$ReasonTypeClasses.ListApply[10];

var ListBind_011 = /* <*> */Apply$ReasonTypeClasses.ListApply[11];

var ListBind_012 = /* applyFirst */Apply$ReasonTypeClasses.ListApply[12];

var ListBind_013 = /* <* */Apply$ReasonTypeClasses.ListApply[13];

var ListBind_014 = /* applySecond */Apply$ReasonTypeClasses.ListApply[14];

var ListBind_015 = /* *> */Apply$ReasonTypeClasses.ListApply[15];

var ListBind = /* module */[
  ListBind_000,
  ListBind_001,
  ListBind_002,
  ListBind_003,
  ListBind_004,
  ListBind_005,
  ListBind_006,
  ListBind_007,
  ListBind_008,
  ListBind_009,
  ListBind_010,
  ListBind_011,
  ListBind_012,
  ListBind_013,
  ListBind_014,
  ListBind_015,
  /* bind */bind$1,
  /* >>= */bind$1,
  /* bindFlipped */bindFlipped$1,
  /* =<< */bindFlipped$1,
  /* join */join$1,
  /* composeKleisli */composeKleisli$1,
  /* >=> */composeKleisli$1,
  /* composeKleisliFlipped */composeKleisliFlipped$1,
  /* <=< */composeKleisliFlipped$1,
  /* ifM */ifM$1
];

exports.MakeBind   = MakeBind;
exports.TestBind   = TestBind;
exports.OptionBind = OptionBind;
exports.ListBind   = ListBind;
/* No side effect */
