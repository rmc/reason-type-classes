open Functor;

module type AltBase = {include Functor; let alt: (t('a), t('a)) => t('a);};

module type Alt = {include AltBase; let (<|>): (t('a), t('a)) => t('a);};

module MakeAlt = (A: AltBase) : (Alt with type t('a) = A.t('a)) => {
  include A;
  let (<|>) = alt;
};

module TestAlt = (A: Alt) => {
  open A;
  let testAltAssociativity = (f, g, h) => {
    let left = f <|> g <|> h;
    let right = f <|> (g <|> h);
    left == right;
  };
  let testAltDistributivity = (f, g, h) => {
    let left = f <$> (g <|> h);
    let right = f <$> g <|> (f <$> h);
    left == right;
  };
};

module OptionAlt: Alt with type t('a) = option('a) =
  MakeAlt(
    {
      include OptionFunctor;
      let alt = (f, g) =>
        switch (f, g) {
        | (Some(f), _) => Some(f)
        | (_, Some(g)) => Some(g)
        | _ => None
        };
    }
  );
