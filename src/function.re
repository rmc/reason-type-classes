let id = (x) => x;
let const = (x, _) => x;
let (<<) = (f, g, x) => f(g(x));
let (>>) = (f, g) => g << f;
