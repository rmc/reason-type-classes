open Applicative;

open Plus;

module type AlternativeBase = {
  type t('a);
  include Applicative with type t('a) := t('a);
  include Plus with type t('a) := t('a);
};

module type Alternative = {include AlternativeBase;};

module MakeAlternative =
       (A: AlternativeBase)
       : (Alternative with type t('a) = A.t('a)) => {
  include A;
};

module TestAlternative = (A: Alternative) => {
  include TestApplicative(A);
  include TestPlus(A);
  open A;
  let testAlternativeDistributivity = (f, g, h) => {
    let left = f <|> g <*> h;
    let right = f <*> h <|> (g <*> h);
    left == right;
  };
  let testAlternativeAnnihilation = f => f <*> empty == empty;
};

module OptionAlternative: Alternative with type t('a) = option('a) =
  MakeAlternative(
    {
      type t('a) = option('a);
      include (OptionApplicative: Applicative with type t('a) := t('a));
      include (OptionPlus: Plus with type t('a) := t('a));
    }
  );
