open Alt;

module type PlusBase = {include Alt; let empty: t('a);};

module type Plus = {include PlusBase;};

module MakePlus = (P: PlusBase) => {
  include P;
};

module TestPlus = (P: Plus) => {
  open P;
  let testPlusLeftIdentity = f => empty <|> f == f;
  let testPlusRightIdentity = f => f <|> empty == f;
  let testPlusAnnihilation = f => f <$> empty == empty;
};

module OptionPlus: Plus with type t('a) = option('a) =
  MakePlus(
    {
      include OptionAlt;
      let empty = None;
    }
  );
