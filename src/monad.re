open Applicative;

open Bind;

module type MonadBase = {
  type t('a);
  include Applicative with type t('a) := t('a);
  include Bind with type t('a) := t('a);
};

module type Monad = {
  include MonadBase;
  let liftM1: ('a => 'b, t('a)) => t('b);
  let ap: (t('a => 'b), t('a)) => t('b);
  let whenM: (t(bool), t(unit)) => t(unit);
  let unlessM: (t(bool), t(unit)) => t(unit);
};

module MakeMonad = (F: MonadBase) : (Monad with type t('a) = F.t('a)) => {
  include F;
  let liftM1 = (f, m) => m >>= (x => f(x) |> pure);
  let ap = (mf, m) => mf >>= (f => m >>= (x => f(x) |> pure));
  let whenM = (mc, m) => mc >>= (c => when_(c, m));
  let unlessM = (mc, m) => mc >>= (c => unless(c, m));
};

module TestMonad = (F: Monad) => {
  include TestApplicative(F);
  include TestBind(F);
  open F;
  let testLeftIdentity = (f, x) => pure(x) >>= f == f(x);
  let testRightIdentity = x => x >>= pure == x;
};

module OptionMonad: Monad with type t('a) = option('a) =
  MakeMonad(
    {
      type t('a) = option('a);
      include (OptionApplicative: Applicative with type t('a) := t('a));
      include (OptionBind: Bind with type t('a) := t('a));
    }
  );
