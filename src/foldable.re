open Monoid;

module type FoldableBase = {
  type t('a);
  let foldr: (('a, 'b) => 'b, 'b, t('a)) => 'b;
  let foldl: (('b, 'a) => 'b, 'b, t('a)) => 'b;
};

module type Foldable = {include FoldableBase;};

module MakeFoldable = (F: FoldableBase) => {
  include F;
};

module type FoldableMonoid = {
  include FoldableBase;
  module M: Monoid;
  let fold: t(M.t) => M.t;
};

module MakeFoldableMonoid =
       (F: FoldableBase, MN: Monoid)
       : (FoldableMonoid with type t('a) = F.t('a) and type M.t = MN.t) => {
  include F;
  module M = MN;
  let fold = f => foldl((b, a) => M.append(b, a), M.mempty, f);
};

module ListFoldable: Foldable with type t('a) = list('a) =
  MakeFoldable(
    {
      type t('a) = list('a);
      let foldr = (f, b, oa) => List.fold_right(f, oa, b);
      let foldl = List.fold_left;
    }
  );

module MakeListFoldableMonoid = (MN: Monoid) =>
  MakeFoldableMonoid(ListFoldable, MN);

module ListFoldableStringMonoid:
  FoldableMonoid with type t('a) = list('a) and type M.t = string =
  MakeListFoldableMonoid(StringMonoid);

module ListFoldableStringMonoidDual:
  FoldableMonoid with type t('a) = list('a) and type M.t = string =
  MakeListFoldableMonoid(Dual.StringMonoidDual);
