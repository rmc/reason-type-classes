open Jest;

open Expect;

describe("Apply", () => {
  open Apply;
  module TestOptionApply = TestApply(OptionApply);
  module TestListApply = TestApply(ListApply);
  describe("OptionApply", () => {
    open TestOptionApply;
    let f = x => x - 1;
    let g = x => x mod 2;
    test("test identity", () =>
      expect(testIdentity(None)) |> toBe(true)
    );
    test("test composition", () =>
      expect(testComposition(f, g, None)) |> toBe(true)
    );
    test("test associative composition", () =>
      expect(testAssociativeComposition(Some(f), Some(g), Some(1)))
      |> toBe(true)
    );
  });
});

describe("Applicative", () => {
  open Applicative;
  module TestOptionApplicative = TestApplicative(OptionApplicative);
  describe("OptionApplicative", () => {
    open TestOptionApplicative;
    let f = x => x - 1;
    let g = x => x mod 2;
    test("test functor identity", () =>
      expect(testIdentity(Some(true))) |> toBe(true)
    );
    test("test functor composition", () =>
      expect(testComposition(f, g, Some(1))) |> toBe(true)
    );
    test("test apply associative composition", () =>
      expect(testAssociativeComposition(Some(f), Some(g), Some(1)))
      |> toBe(true)
    );
    test("test applicative identity", () =>
      expect(testApplicativeIdentity(Some(true))) |> toBe(true)
    );
    test("test applicative composition", () =>
      expect(testApplicativeComposition(Some(f), Some(g), Some(1)))
      |> toBe(true)
    );
    test("test applicative homomorphism", () =>
      expect(testApplicativeHomomorphism(f, 1)) |> toBe(true)
    );
    test("test applicative interchange", () =>
      expect(testApplicativeInterchange(Some(f), 1)) |> toBe(true)
    );
  });
});

describe("Bind", () => {
  open Bind;
  module TestOptionBind = TestBind(OptionBind);
  describe("OptionBind", () => {
    open TestOptionBind;
    let f = x => x - 1;
    let g = x => x mod 2;
    test("test identity", () => {
      expect(testIdentity(Some(true))) |> toBe(true) |> ignore;
      expect(testIdentity(None)) |> toBe(true);
    });
    test("test composition", () => {
      expect(testComposition(f, g, Some(1))) |> toBe(true) |> ignore;
      expect(testComposition(f, g, None)) |> toBe(true);
    });
    test("test associative composition", () => {
      expect(testAssociativeComposition(Some(f), Some(g), Some(1)))
      |> toBe(true)
      |> ignore;
      expect(testAssociativeComposition(Some(f), Some(g), None)) |> toBe(true);
    });
    test("test bind associativity", () => {
      let f = x => x mod 2 == 0 ? Some(x) : None;
      let g = x => x + 1 > 2 ? Some(x) : None;
      expect(testBindAssociativity(f, g, Some(3))) |> toBe(true);
    });
  });
});

describe("Monad", () => {
  open Monad;
  module TestOptionMonad = TestMonad(OptionMonad);
  describe("OptionMonad", () => {
    open TestOptionMonad;
    let f = b => b ? Some(b) : None;
    test("left identity", () => {
      expect(testLeftIdentity(f, true)) |> toBe(true) |> ignore;
      expect(testLeftIdentity(f, false)) |> toBe(true);
    });
    test("right identity", () =>
      expect(testRightIdentity(Some(1))) |> toBe(true)
    );
  });
});

describe("Alt", () => {
  open Alt;
  module TestOptionAlt = TestAlt(OptionAlt);
  describe("OptionAlt", () =>
    TestOptionAlt.(
      test("associativity", () =>
        testAltAssociativity(Some(1), Some(5), Some(8)) |> expect |> toBe(true)
      )
    )
  );
});

describe("Plus", () => {
  open Plus;
  module TestOptionPlus = TestPlus(OptionPlus);
  describe("OptionPlus", () => {
    open TestOptionPlus;
    test("left identity", () =>
      testPlusLeftIdentity(Some(1)) |> expect |> toBe(true)
    );
    test("right identity", () =>
      testPlusRightIdentity(Some(1)) |> expect |> toBe(true)
    );
    test("annihilation", () =>
      testPlusAnnihilation((_) => Some(1)) |> expect |> toBe(true)
    );
  });
});

describe("Alternative", () => {
  open Alternative;
  module TestOptionAlternative = TestAlternative(OptionAlternative);
  describe("OptionAlternative", () => {
    open TestOptionAlternative;
    let f = x => x mod 2 == 0;
    let g = x => x + 1 > 2;
    test("distributivity", () =>
      testAlternativeDistributivity(Some(f), Some(g), Some(1))
      |> expect
      |> toBe(true)
    );
    test("annihilation", () =>
      testAlternativeAnnihilation(Some(f)) |> expect |> toBe(true)
    );
  });
});
