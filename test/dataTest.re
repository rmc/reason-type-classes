open Jest;

open Expect;

describe("Monoid", () => {
  open Monoid;
  module TestStringMonoid = TestMonoid(StringMonoid);
  describe("StringMonoid", () => {
    test("test left identity", () =>
      TestStringMonoid.testLeftIdentity("a") |> expect |> toBe(true)
    );
    test("test right identity", () =>
      TestStringMonoid.testRightIdentity("a") |> expect |> toBe(true)
    );
  });
  describe("Dual", () => {
    open Dual;
    let first = "first";
    let second = "second";
    test("string dual", () =>
      StringMonoidDual.append(first, second)
      |> expect
      |> toBe(StringMonoid.append(second, first))
    );
  });
  describe("Additive", () =>
    Additive.(
      test("int additive append", () =>
        IntMonoidAdditive.append(Additive(3), Additive(5))
        |> expect
        |> toEqual(Additive(8))
      )
    )
  );
  describe("Multiplicative", () =>
    Multiplicative.(
      test("int multiplicative append", () =>
        IntMonoidMultiplicative.append(Multiplicative(3), Multiplicative(5))
        |> expect
        |> toEqual(Multiplicative(15))
      )
    )
  );
  describe("Conj", () => {
    open Conj;
    test("append true false", () =>
      BoolMonoidConj.append(Conj(true), Conj(false))
      |> expect
      |> toEqual(Conj(false))
    );
    test("append false false", () =>
      BoolMonoidConj.append(Conj(false), Conj(false))
      |> expect
      |> toEqual(Conj(false))
    );
    test("append true true", () =>
      BoolMonoidConj.append(Conj(true), Conj(true))
      |> expect
      |> toEqual(Conj(true))
    );
    test("append false true", () =>
      BoolMonoidConj.append(Conj(false), Conj(true))
      |> expect
      |> toEqual(Conj(false))
    );
  });
  describe("Disj", () => {
    open Disj;
    test("append true false", () =>
      BoolMonoidDisj.append(Disj(true), Disj(false))
      |> expect
      |> toEqual(Disj(true))
    );
    test("append false false", () =>
      BoolMonoidDisj.append(Disj(false), Disj(false))
      |> expect
      |> toEqual(Disj(false))
    );
    test("append true true", () =>
      BoolMonoidDisj.append(Disj(true), Disj(true))
      |> expect
      |> toEqual(Disj(true))
    );
    test("append false true", () =>
      BoolMonoidDisj.append(Disj(false), Disj(true))
      |> expect
      |> toEqual(Disj(true))
    );
  });
});

describe("Functor", () => {
  open Functor;
  module TestOptionFunctor = TestFunctor(OptionFunctor);
  module TestListFunctor = TestFunctor(ListFunctor);
  describe("OptionFunctor", () => {
    let f = x => x - 1;
    let g = x => x mod 2;
    open TestOptionFunctor;
    test("test identity", () => {
      expect(testIdentity(Some(true))) |> toBe(true) |> ignore;
      expect(testIdentity(None)) |> toBe(true);
    });
    test("test composition", () => {
      expect(testComposition(f, g, Some(1))) |> toBe(true) |> ignore;
      expect(testComposition(f, g, None)) |> toBe(true);
    });
  });
});
