# Type Classes in ReasonML

Based on PureScript

# Build

```
yarn build
```

# Build + Watch

```
yarn start
```

# Test + Watch

```
yarn test --watch
```
